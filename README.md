# Library

[https://vkainth.gitlab.io/js-library-project](https://vkainth.gitlab.io/js-library-project)

## Introduction

A simple library application that uses the
[Local Storage](https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage)
API to help you manage your reading library.

## Features

- Uses Local Storage to hold all data

- Uses Bootstrap Web Framework for a pleasing aesthetic

- Uses vanilla JavaScript only

- Uses regular expressions in HTML forms for input validation

- Very, very lightweight

- Currently, only Title, Author, Number of Pages, and Read Status are stored

- Uses a template in HTML which is hidden by default and cloned for every book element

## Installation

- Clone or download the repository

- Launch `index.html` from the **public** folder to view the Library offline

## Future Enhancements

- Add and save book images
