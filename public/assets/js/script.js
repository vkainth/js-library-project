const libraryContainer = document.querySelector('.album');
const libraryRow = libraryContainer.querySelector('.row');
const formElement = document.querySelector('form');
const titleElement = formElement.querySelector('input[name=title]');
const authorElement = formElement.querySelector('input[name=author]');
const pagesElement = formElement.querySelector('input[name=pages]');
const readElement = formElement.querySelector('input[name=read]');
const addBookBtn = formElement.querySelector('.btn-primary');

let library = [];
let bookId = 0;

const bookFactory = (id, title, author, pages, read) => {
    return { id, title, author, pages, read };
};

const addSampleData = () => {
    let book = bookFactory(
        bookId,
        'Wheel of Time',
        'Robert Jordan',
        1000,
        true
    );
    addBookToLibrary(book);
    book = bookFactory(
        bookId,
        'Wise Man\'s Fear',
        'Patrick Rothfuss',
        1200,
        true
    );
    addBookToLibrary(book);
    book = bookFactory(
        bookId,
        'The Fellowship of The Ring',
        'J.R.R Tolkien',
        800,
        true
    );
    addBookToLibrary(book);
    book = bookFactory(bookId, 'The Two Towers', 'J.R.R Tolkien', 600, true);
    addBookToLibrary(book);
    book = bookFactory(bookId, 'The Dark Tower', 'Stephen King', 800, true);
    addBookToLibrary(book);
};

const addBookToLibrary = (aBook = null) => {
    let book;
    if (aBook) {
        book = aBook;
    } else {
        book = bookFactory(
            bookId,
            titleElement.value,
            authorElement.value,
            Number(pagesElement.value),
            Boolean(readElement.checked)
        );
    }
    bookId += 1;
    library.push(book);
    saveToLocalStorage();
};

const createBookElement = book => {
    const bookTemplate = document.querySelector('#bookTemplate');
    const newBook = bookTemplate.cloneNode(true);
    newBook.setAttribute('id', `book-${book.id}`);
    newBook.style = '';
    const bookTitle = newBook.querySelector('h5');
    const bookAuthor = newBook.querySelector('h6');
    const bookPages = newBook.querySelector('small');
    const bookRead = newBook.querySelectorAll('button')[0];
    const bookDelete = newBook.querySelectorAll('button')[1];
    bookTitle.textContent = book.title;
    bookAuthor.textContent = book.author;
    bookPages.textContent = `${book.pages} pages`;
    if (book.read) {
        bookRead.classList.add('btn-outline-success');
    }
    const card = newBook.querySelector('.card');
    card.addEventListener('mouseover', onBookEnter);
    card.addEventListener('mouseout', onBookLeave);
    bookRead.addEventListener('click', readBookUpdate);
    libraryRow.appendChild(newBook);
    bookDelete.addEventListener('click', deleteBookElement);
};

const onBookEnter = event => {
    event.currentTarget.classList.add('shadow');
};

const onBookLeave = event => {
    event.currentTarget.classList.remove('shadow');
};

const readBookUpdate = event => {
    event.currentTarget.classList.toggle('btn-outline-success');
    let book = findBook(
        event.currentTarget.parentNode.parentNode.parentNode.parentNode.parentNode.getAttribute(
            'id'
        )
    );
    book.read = !book.read;
    saveToLocalStorage();
};

const findBook = id => {
    return library.find(book => {
        return `book-${book.id}` == id;
    });
};

const deleteBookElement = event => {
    let book = findBook(
        event.currentTarget.parentNode.parentNode.parentNode.parentNode.parentNode.getAttribute(
            'id'
        )
    );
    library.splice(library.indexOf(book), 1);
    saveToLocalStorage();
    window.location.reload(true);
};

const renderBooks = () => {
    for (let book of library) {
        createBookElement(book);
    }
};

addBookBtn.addEventListener('click', function() {
    if (formElement.checkValidity()) {
        addBookToLibrary();
    }
});

window.onload = function() {
    fetchFromLocalStorage();
    if (library.length === 0) {
        addSampleData();
    }
    renderBooks();
};

const fetchFromLocalStorage = () => {
    library = JSON.parse(window.localStorage.getItem('library')) || [];
    bookId = Number(window.localStorage.getItem('currentId')) || 0;
};

const saveToLocalStorage = () => {
    window.localStorage.setItem('library', JSON.stringify(library));
    window.localStorage.setItem('currentId', bookId);
};
